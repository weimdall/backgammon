var pseudos = ['', ''];
var valeurs = [0,0,0,0];
var tableau = [0, -2, 0, 0, 0, 0, 5, 0, 3, 0, 0, 0, -5, 5, 0, 0, 0, -3, 0, -5, 0, 0, 0, 0, 2, 0];
var joueur = 1;
var joueurTour = 1;
var flecheDepart = 0;
var flecheArrivee = 0;
const ordreFreches = ["#exit_2", "#case_2_13", "#case_2_12", "#case_2_11", "#case_2_10", "#case_2_9", "#case_2_8",
                    "#case_2_6", "#case_2_5", "#case_2_4", "#case_2_3", "#case_2_2", "#case_2_1",
                    "#case_1_1", "#case_1_2", "#case_1_3", "#case_1_4", "#case_1_5", "#case_1_6",
                    "#case_1_8", "#case_1_9", "#case_1_10", "#case_1_11", "#case_1_12", "#case_1_13", "#exit_1"];
const barre = ["#case_2_7", "#case_1_7"];
const pion = ["#pion1", "#pion2"];
const faces = ['face face-1', 'face face-2','face face-3', 'face face-4', 'face face-5', 'face face-6'];

const EFFACE = -2;

function addPion(fleche, joueur)
{
    if(fleche < 1 || joueur < 1 || fleche > 25 || joueur > 2)
        return;
    if(fleche < 12)
        $(ordreFreches[fleche]).prepend($(pion[joueur-1]).html());
    else
        $(ordreFreches[fleche]).append($(pion[joueur-1]).html());
}

function sendAction(fleche, valeur)
{
    $.ajax({
        type:'POST',
        url:'action',
        data:'fleche='+fleche+'&valeur='+valeur,
        dataType:'json',
        success:function(resp){
            if(resp == 'false') {
                alert('Nope');
                RefreshServer();
            }
            RefreshGame();
        }
    })
}

function RefreshServer()
{
    $.ajax({
        type:'POST',
        url:'refresh',
        dataType:'json',
        success:function(resp){
            tableau = resp.tableau;
            valeurs = resp.valeurs;
            joueurTour = resp.joueurTour;
            joueur = resp.joueur;
            pseudos = resp.pseudos;
            if(joueurTour != joueur)
                setTimeout(RefreshServer, 2000);
            RefreshGame();
        }
    })
}

function RefreshGame()
{
    //Nettoyage
    for (var i = 0; i < 26; i++)
        $(ordreFreches[i]).empty();
    for (i = 0; i < 2; i++)
        $(barre[i]).empty();
    $('#infoJ1').empty();
    $('#infoJ2').empty();
    $('#tour').empty();

    //Remplissage des infos
    $('#infoJ1').append($(pion[0]).html()).append(': '+pseudos[0]);
    $('#infoJ2').append($(pion[1]).html()).append(': '+pseudos[1]);
    $('#tour').append('Tour de :').append($(pion[joueurTour-1]).html());

    //Affichage des dés
    $('#de1').attr('class', '').addClass(faces[valeurs[0]-1]);
    $('#de2').attr('class', '').addClass(faces[valeurs[1]-1]);

    //Remplissage du plateau
    for (i = 1; i <= 24; i++)
    {
        var joueur;
        if(tableau[i] < 0)
            joueur = 2;
        else
            joueur = 1;
        for (var j = 0; j < Math.abs(tableau[i]); j++)
            addPion(i, joueur);
    }

    //Remplissage de la barre
    for (i = 0; i < Math.abs(tableau[0]); i++)
        $(barre[0]).append($(pion[0]).html());
    for (i = 0; i < Math.abs(tableau[25]); i++)
        $(barre[1]).append($(pion[1]).html());

}

//depart = EFFACE -> Annule le surlignement
function SurligneCase(depart) {
    if(depart == EFFACE)
    {
        ordreFreches.forEach(function (val) {
            $(val).removeClass("boxSelect");
        });
        return;
    }
    valeurs.forEach(function (val) {
        if(val != 0)
        {
            if(joueur == 1)
                $(ordreFreches[depart + val]).addClass("boxSelect");
            else if(joueur == 2)
                $(ordreFreches[ depart - val]).addClass("boxSelect");
        }
    });
}

function dragPiece(event)
{
    //Verif tour du joueur
    if(joueurTour != joueur)
        return false;

    flecheDepart = ordreFreches.indexOf("#".concat(event.originalTarget.parentNode.attributes['id'].value));
    //Verification de la barre
    if(flecheDepart == -1) {
        if (joueur == 1 && tableau[0] != 0)
            flecheDepart = -1;
        if (joueur == 2 && tableau[25] != 0 )
            flecheDepart = 24;
        SurligneCase(flecheDepart);
        return true;
    }

    console.log("Flèche de départ : ");
    console.log(flecheDepart);

    //Verif pion du joueur
    var facteur = (joueur == 1) ? 1 : -1;
    if(tableau[flecheDepart]*facteur > 0) {
        SurligneCase(flecheDepart);
        return true;
    }
    else {
        console.log("Vous ne pouvez pas jouer ce pion");
        return false;
    }
}

function dragPieceEnd(event)
{
    SurligneCase(EFFACE);
    flecheArrivee = 0;
    flecheDepart = 0;
    return true;
}

function allowDrop(event) {
    event.preventDefault();
}

function dropPiece(event) {
    event.preventDefault();
    console.log("Flèche d'arrivée : ");
    flecheArrivee = ordreFreches.indexOf("#".concat(event.target.attributes['id'].value));
    console.log(flecheArrivee);

    var facteur = (joueur == 1) ? 1 : -1;
    if(tableau[flecheDepart]*facteur > 0 &&
        tableau[flecheArrivee]*facteur < 5 &&
        tableau[flecheArrivee]*facteur > -2)
    {
        var find = false;

        //Si le saut est dans les valeurs jouable
        if(joueur == 1 && valeurs.indexOf(flecheArrivee-flecheDepart) != -1)
        {
            find = true;
            valeurs[valeurs.indexOf(flecheArrivee-flecheDepart)] = 0;
        }
        else if(joueur == 2 && valeurs.indexOf(flecheDepart-flecheArrivee) != -1)
        {
            find = true;
            valeurs[valeurs.indexOf(flecheDepart-flecheArrivee)] = 0;
        }


        if(find) {
            //----
            //Pion à mettre sur la barre
            if(tableau[flecheArrivee]*facteur == -1)
            {
                tableau[flecheArrivee]++;
                if(joueur == 1)
                    tableau[25]-=facteur;
                if(joueur == 2)
                    tableau[0]-=facteur;
            }
            //----

            tableau[flecheDepart]-=facteur;
            tableau[flecheArrivee]+=facteur;
            RefreshGame();
            if(joueur == 1)
                sendAction(flecheDepart, flecheArrivee-flecheDepart);
            else
                sendAction(flecheDepart, flecheDepart-flecheArrivee);
            RefreshServer();
            return true;
        }
        else
            return false;
    }
    else
        return false;

}

