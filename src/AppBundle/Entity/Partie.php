<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Partie
 *
 * @ORM\Table(name="Partie", indexes={@ORM\Index(name="FK_Partie_ID_Joueur", columns={"ID_Joueur"}), @ORM\Index(name="FK_Partie_ID_Joueur_1", columns={"ID_Joueur_1"})})
 * @ORM\Entity
 */
class Partie
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure", type="datetime", nullable=false)
     */
    private $heure = 'CURRENT_TIMESTAMP';

    /**
     * @return \DateTime
     */
    public function getHeure()
    {
        return $this->heure;
    }

    /**
     * @param \DateTime $heure
     */
    public function setHeure($heure)
    {
        $this->heure = $heure;
    }


    /**
     * @var integer
     *
     * @ORM\Column(name="vainqueur", type="integer", nullable=true)
     */
    private $vainqueur = null;

    /**
     * @return int
     */
    public function getVainqueur()
    {
        return $this->vainqueur;
    }

    /**
     * @param int $vainqueur
     */
    public function setVainqueur($vainqueur)
    {
        $this->vainqueur = $vainqueur;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="score", type="integer", nullable=false)
     */
    private $score = 1;

    /**
     * @return int
     */
    public function getScore()
    {
        return $this->vainqueur;
    }

    /**
     * @param int $score
     */
    public function setScore($score)
    {
        $this->score = $score;
    }


    /**
     * @var \AppBundle\Entity\Joueur
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Joueur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_Joueur", referencedColumnName="ID")
     * })
     */

    private $idJoueur = null;

    /**
     * @return Joueur
     */
    public function getIdJoueur1()
    {
        return $this->idJoueur;
    }

    /**
     * @param Joueur $idJoueur
     */
    public function setIdJoueur1($idJoueur)
    {
        $this->idJoueur = $idJoueur;
    }

    /**
     * @var \AppBundle\Entity\Joueur
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Joueur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_Joueur_1", referencedColumnName="ID")
     * })
     */
    private $idJoueur1 = null;

    /**
     * @return Joueur
     */
    public function getIdJoueur2()
    {
        return $this->idJoueur1;
    }

    /**
     * @param Joueur $idJoueur
     */
    public function setIdJoueur2($idJoueur)
    {
        $this->idJoueur1 = $idJoueur;
    }


}

