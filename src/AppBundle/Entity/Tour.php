<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tour
 *
 * @ORM\Table(name="Tour", indexes={@ORM\Index(name="FK_Tour_ID_Partie", columns={"ID_Partie"}), @ORM\Index(name="FK_Tour_ID_Joueur", columns={"ID_Joueur"})})
 * @ORM\Entity
 */
class Tour
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="De1", type="integer", nullable=false)
     */
    private $de1;

    /**
     * @return int
     */
    public function getDe1()
    {
        return $this->de1;
    }

    /**
     * @param int $de1
     */
    public function setDe1($de1)
    {
        $this->de1 = $de1;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="De2", type="integer", nullable=false)
     */
    private $de2;

    /**
     * @return int
     */
    public function getDe2()
    {
        return $this->de2;
    }

    /**
     * @param int $de2
     */
    public function setDe2($de2)
    {
        $this->de2 = $de2;
    }


    /**
     * @var \AppBundle\Entity\Joueur
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Joueur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_Joueur", referencedColumnName="ID")
     * })
     */
    private $idJoueur;

    /**
     * @return Joueur
     */
    public function getIdJoueur()
    {
        return $this->idJoueur;
    }

    /**
     * @param Joueur $idJoueur
     */
    public function setIdJoueur($idJoueur)
    {
        $this->idJoueur = $idJoueur;
    }

    /**
     * @var \AppBundle\Entity\Partie
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Partie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_Partie", referencedColumnName="ID")
     * })
     */
    private $idPartie;

    /**
     * @return Partie
     */
    public function getIdPartie()
    {
        return $this->idPartie;
    }

    /**
     * @param Partie $idPartie
     */
    public function setIdPartie($idPartie)
    {
        $this->idPartie = $idPartie;
    }



}

