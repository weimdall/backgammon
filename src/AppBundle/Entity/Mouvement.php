<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mouvement
 *
 * @ORM\Table(name="Mouvement", indexes={@ORM\Index(name="FK_Mouvement_ID_Tour", columns={"ID_Tour"})})
 * @ORM\Entity
 */
class Mouvement
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="Valeur", type="integer", nullable=false)
     */
    private $valeur;

    /**
     * @param int $valeur
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;
    }

    /**
     * @return int
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="Fleche", type="integer", nullable=false)
     */
    private $fleche;

    /**
     * @return int
     */
    public function getFleche()
    {
        return $this->fleche;
    }

    /**
     * @param int $fleche
     */
    public function setFleche($fleche)
    {
        $this->fleche = $fleche;
    }

    /**
     * @var \AppBundle\Entity\Tour
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Tour")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_Tour", referencedColumnName="ID")
     * })
     */
    private $idTour;

    /**
     * @param Tour $idTour
     */
    public function setIdTour($idTour)
    {
        $this->idTour = $idTour;
    }

    /**
     * @return Tour
     */
    public function getIdTour()
    {
        return $this->idTour;
    }

}

