<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 01/09/16
 * Time: 03:04
 */

namespace AppBundle\Model;

use AppBundle\Entity\Joueur;
use AppBundle\Entity\Mouvement;
use AppBundle\Entity\Partie;
use AppBundle\Entity\Tour;
use \Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Component\Config\Definition\Exception\Exception;


class BackgammonModel
{
    private $partie = null;
    private $manager = null;
    private $tableau = [];
    private $tourActuel = null;
    function __construct(Doctrine $doctrine, $id = null)
    {
        $this->tableau = [0, -2, 0, 0, 0, 0, 5, 0, 3, 0, 0, 0, -5, 5, 0, 0, 0, -3, 0, -5, 0, 0, 0, 0, 2, 0];
        $this->manager = $doctrine->getManager();
        if(is_null($id))
        {
            $this->partie = new Partie();
            $this->partie->setHeure(new \DateTime());
            $this->manager->persist($this->partie);
            $this->manager->flush();
        }
        else
        {
            $this->partie = $this->manager->getRepository('AppBundle:Partie')->find($id);
            if($this->partie == null)
                throw new Exception("IDPartie doesn't exist.", 2);
            $this->tourActuel = $this->manager->getRepository('AppBundle:Tour')->findOneBy(   array('idPartie'=> $this->partie->getId()),
                array('id' => 'DESC'));
        }

    }

    function getId()
    {
        return $this->partie->getId();
    }

    /**
     * @return \DateTime
     */
    function getHeure()
    {
        return $this->partie->getHeure();
    }

    function LoadTable()
    {
        $this->tableau = [0, -2, 0, 0, 0, 0, 5, 0, 3, 0, 0, 0, -5, 5, 0, 0, 0, -3, 0, -5, 0, 0, 0, 0, 2, 0];

        $tours = $this->manager->getRepository('AppBundle:Tour')->findBy(   array('idPartie'=> $this->partie->getId()),
                                                                            array('id' => 'ASC'));
        foreach ($tours as $tour)
        {
            $mouvements = $this->manager->getRepository('AppBundle:Mouvement')->findBy( array('idTour'=> $tour->getId()),
                                                                                        array('id' => 'ASC'));
            foreach ($mouvements as $mouvement)
                $this->Joue($mouvement, $tour->getIdJoueur());
        }
    }

    private function Joue(Mouvement $mouvement, Joueur $joueur)
    {
        if($this->partie->getIdJoueur1()->getId() == $joueur->getId())
        {
            $nJoueur = 1;
            $facteur = 1;
        }
        else
        {
            $nJoueur = 2;
            $facteur = -1;
        }

        $depart = $mouvement->getFleche();
        $arrivee = $mouvement->getFleche()+($mouvement->getValeur()*$facteur);
        if($this->tableau[$arrivee] == $facteur*-1)
        {
            $this->tableau[$arrivee] += $facteur;
            if($nJoueur == 1)
                $this->tableau[25]--;
            else
                $this->tableau[0]++;
        }
        $this->tableau[$depart] -= $facteur;
        if($arrivee != 25 && $arrivee != 0)
            $this->tableau[$arrivee] += $facteur;
        else
            $this->Vainqueur();
    }

    public function Jouable(Mouvement $mouvement, Joueur $joueur)
    {
        $tour = $this->manager->getRepository('AppBundle:Tour')->find($mouvement->getIdTour()->getId());
        if($this->partie->getVainqueur() != null)
            return false;
        if(!in_array($mouvement->getValeur(), $this->getValeurs()))
            return false;

        if($tour->getIdJoueur()->getId() != $joueur->getId())
            return false;

        if($this->partie->getIdJoueur1()->getId() == $joueur->getId())
        {
            $nJoueur = 1;
            $facteur = 1;
        }
        else
        {
            $nJoueur = 2;
            $facteur = -1;
        }
        $depart = $mouvement->getFleche();
        $arrivee = $mouvement->getFleche()+($mouvement->getValeur()*$facteur);
        if($arrivee > 25 || $arrivee < 0)
            return false;
        $barre = 0;
        if($nJoueur == 2)
            $barre = 25;

        if($this->tableau[$barre] != 0 && $mouvement->getFleche() != $barre)
            if($this->tableau[$barre+($facteur*$mouvement->getValeur())]*$facteur >= -1 ||
                $this->tableau[$barre+($facteur*$mouvement->getValeur())]*$facteur < 5)
                return false;

        if($this->tableau[$depart]*$facteur < 1)
            return false;

        if($arrivee == 25 || $arrivee == 0)
        {
            //Tentative de sortie de pion
            if($nJoueur == 1)
                for ($i = 0 ; $i < 19 ; $i++)
                    if($this->tableau[$i] > 0)
                        return false;
            if($nJoueur == 2)
                for ($i = 25 ; $i > 6 ; $i--)
                    if($this->tableau[$i] < 0)
                        return false;
            return true;
        }

        if($this->tableau[$arrivee]*$facteur >= 5)
            return false;
        if($this->tableau[$arrivee]*$facteur < -1)
            return false;

        return true;

    }

    function FinTour(Tour $tour = null)
    {
        if(is_null($tour))
            $tour = $this->tourActuel;
        if(is_null($tour))
            return false;

        $mouvements = $this->manager->getRepository('AppBundle:Mouvement')->findBy( array('idTour'=> $tour->getId()),
            array('id' => 'DESC'));
        $c = count($mouvements);

        if($tour->getDe1() == $tour->getDe2())
            $fin = ($c == 4);
        else
            $fin = ($c == 2);
        if($fin)
            return true;

        $m = new Mouvement();
        $m->setIdTour($tour);
        foreach ($this->getValeurs() as $valeur)
        {
            if($valeur == 0)
                continue;
            $m->setValeur($valeur);
            for ($i = 0 ; $i < 25 ; $i++)
            {
                $m->setFleche($i);
                if($this->Jouable($m, $tour->getIdJoueur()))
                    return false;
            }
        }

        return true;
    }

    function NouveauTour()
    {
        if(!is_null($this->tourActuel) && !$this->FinTour())
            return false;

        $tour = new Tour();
        if($this->tourActuel == null)
            $tour->setIdJoueur($this->partie->getIdJoueur1());
        else if($this->partie->getIdJoueur1()->getId() == $this->tourActuel->getIdJoueur()->getId())
            $tour->setIdJoueur($this->partie->getIdJoueur2());
        else
            $tour->setIdJoueur($this->partie->getIdJoueur1());

        $tour->setDe1(random_int(1,6));
        $tour->setDe2(random_int(1,6));
        $tour->setIdPartie($this->partie);

        $this->manager->persist($tour);
        $this->manager->flush();
        $this->tourActuel = $tour;
        return true;
    }

    function NouveauMouvement($fleche, $valeur, $joueur)
    {
        $mouvement = new Mouvement();
        $tour = $this->tourActuel;

        $mouvement->setIdTour($tour);
        $mouvement->setFleche($fleche);
        $mouvement->setValeur($valeur);

        if(!$this->Jouable($mouvement, $joueur))
            return false;

        $this->Joue($mouvement, $tour->getIdJoueur());
        $this->partie->setHeure(new \DateTime());
        $this->manager->persist($mouvement);
        $this->manager->persist($this->partie);
        $this->manager->flush();
        while($this->FinTour())
            $this->NouveauTour();
        return true;
    }

    function Vainqueur()
    {
        $p = [0,0];
        for ($i = 0 ; $i < 26 ; $i++)
        {
            if($this->tableau[$i] > 0)
                $p[0]++;
            elseif ($this->tableau[$i] < 0)
                $p[1]++;
            if($p[0] != 0 && $p[1] != 0)
                break;
        }
        for ($i = 0 ; $i < 2 ; $i++)
        {
            if ($p[$i] == 0)
            {
                $this->partie->setVainqueur($i+1);
                $this->manager->persist($this->partie);
                $this->manager->flush();
            }
        }
        return $this->partie->getVainqueur();
    }

    public function addJoueur(Joueur $j)
    {
        if($this->isReady())
            return;

        if($this->partie->getIdJoueur1() == null)
            $this->partie->setIdJoueur1($j);
        else if($this->partie->getIdJoueur2() == null)
            $this->partie->setIdJoueur2($j);
        $this->manager->persist($this->partie);
        $this->manager->flush();
    }

    public function getJoueurs()
    {
        return [$this->partie->getIdJoueur1(), $this->partie->getIdJoueur2()];
    }

    public function isReady()
    {
        return !is_null($this->partie->getIdJoueur1()) && !is_null($this->partie->getIdJoueur2());
    }

    /**
     * @return array
     */
    public function getTableau()
    {
        return $this->tableau;
    }

    public function getValeurs()
    {
        $tour = $this->tourActuel;
        $valeur = [$tour->getDe1(), $tour->getDe2(), 0, 0];
        if($tour->getDe1() == $tour->getDe2())
        {
            $valeur[2] = $tour->getDe1();
            $valeur[3] = $tour->getDe1();
        }

        $mouvements = $this->manager->getRepository('AppBundle:Mouvement')->findBy( array('idTour'=> $tour->getId()),
            array('id' => 'DESC'));
        foreach ($mouvements as $mouvement)
        {
            for($i = 3 ; $i >= 0 ; $i--)
                if($valeur[$i] == $mouvement->getValeur())
                {
                    $valeur[$i] = 0;
                    break;
                }
        }

        return $valeur;
    }

    /**
     * @return Tour
     */
    public function getTourActuel()
    {
        return $this->tourActuel;
    }
}