<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 08/09/16
 * Time: 21:35
 */

namespace AppBundle\Model;

use AppBundle\Entity\Joueur;
use AppBundle\Entity\Partie;
use \Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Session\SessionInterface as Session;

class MatchmakingModel
{
    public static function getModel(Doctrine $doctrine, Session $session, Joueur $user)
    {
        $dateLim = (new \DateTime())->modify('-5 minute');
        $idPartie = $session->get('idPartie');
        if($idPartie != null)
        {
            try
            {
                $model = new BackgammonModel($doctrine, $idPartie);
            }catch (\Exception $e){
                $model = null;
            }
            if(!is_null($model) && $model->Vainqueur() == null && $model->isReady())
            {
                if($model->getHeure()->diff((new \DateTime()), true) < $dateLim)
                    return $model;
                else
                {
                    $partie = $doctrine->getManager()->getRepository('AppBundle:Partie')->find($idPartie);
                    $partie->setVainqueur(0);
                    $doctrine->getManager()->persist($partie);
                    $doctrine->getManager()->flush();
                }
            }
        }

        $partie = $doctrine->getManager()->getRepository('AppBundle:Partie')->findOneBy(array('idJoueur' => null), array('id' => 'ASC'));
        if(is_null($partie))
        {
            $partie = $doctrine->getManager()->getRepository('AppBundle:Partie')->findOneBy(array('idJoueur1' => null), array('id' => 'ASC'));
            if($partie->getIdJoueur1()->getId() != $user->getId())
            {
                $newPartie = new Partie();
                $newPartie->setHeure(new \DateTime());
                $doctrine->getManager()->persist($newPartie);
                $doctrine->getManager()->flush();
                if(is_null($partie))
                    $partie = $newPartie;
            }
        }
        $model = new BackgammonModel($doctrine, $partie->getId());
        if($model->getJoueurs()[0] != $user)
            $model->addJoueur($user);
        $session->set('idPartie', $model->getId());
        return $model;
    }
}