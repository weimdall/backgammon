<?php
/**
 * Created by PhpStorm.
 * User: julien
 * Date: 29/08/16
 * Time: 23:19
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Joueur;
use AppBundle\Entity\Mouvement;
use AppBundle\Entity\Partie;
use AppBundle\Model\BackgammonModel;
use AppBundle\Model\MatchmakingModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * @Route("/backgammon", name="backgammonGame")
 */
class BackgammonController extends Controller
{

    /**
     * @Route("/play", name="playpage")
     */
    public function playAction(Request $request)
    {
        $idPartie = $request->getSession()->get('idPartie');
        if($idPartie != null)
        {
            try
            {
                $model = new BackgammonModel($this->getDoctrine(), $idPartie);
                if ($model->Vainqueur() != null || !$model->isReady())
                    return $this->redirect('matchmaking');
            }catch (\Exception $e)
            {
                $request->getSession()->set('idPartie', null);
                return $this->redirect('matchmaking');
            }
        }
        return $this->render('play.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    /**
     * @Route("/matchmaking", name="matchmakingpage")
     */
    public function matchmakingAction(Request $request)
    {
        $model = MatchmakingModel::getModel($this->getDoctrine(), $request->getSession(), $this->getUser());
        if($model->isReady())
            $model->NouveauTour();

        return $this->render('matchmaking.html.twig',
                ['base_dir' => realpath($this->getParameter('kernel.root_dir') . '/..'),]);

    }

    /**
     * @Route("/check", name="checkMatchJson")
     */
    public function checkAction(Request $request)
    {
        $idPartie = $request->getSession()->get('idPartie');
        for($i = 0 ; $i < 30 ; $i++)
        {
            try
            {
                $model = new BackgammonModel($this->getDoctrine(), $idPartie);
            }catch (\Exception $e){
                break;
            }
            if($model->isReady())
                return new JsonResponse('true');
            sleep(1);
        }
        return new JsonResponse('false');
    }

    /**
     * @Route("/refresh", name="refreshJson")
     */
    public function refreshAction(Request $request)
    {

        $idPartie = $request->getSession()->get('idPartie');
        $model = new BackgammonModel($this->getDoctrine(), $idPartie);
        $model->LoadTable();
        return new JsonResponse(array('tableau' => $model->getTableau(),
                'valeurs' => $model->getValeurs(),
                'joueur' => ($this->getUser() == $model->getJoueurs()[0]) ? 1 : 2,
                'joueurTour' => ($model->getTourActuel()->getIdJoueur() == $model->getJoueurs()[0]) ? 1 : 2,
                'pseudos'    => [$model->getJoueurs()[0]->getUsername(), $model->getJoueurs()[1]->getUsername()]));
    }

    /**
     * @Route("/action", name="actionJson")
     */
    public function actionAction(Request $request)
    {
        if(!isset($_POST['fleche']) || !isset($_POST['valeur']) || $_POST['valeur'] == 0)
            return new JsonResponse('false');

        $fleche = filter_input(INPUT_POST, 'fleche', FILTER_SANITIZE_NUMBER_INT);
        $valeur = filter_input(INPUT_POST, 'valeur', FILTER_SANITIZE_NUMBER_INT);
        $idPartie = $request->getSession()->get('idPartie');
        $model = new BackgammonModel($this->getDoctrine(), $idPartie);
        $model->LoadTable();

        return new JsonResponse(($model->NouveauMouvement($fleche, $valeur, $this->getUser())) ? 'true' : 'false');
    }
}