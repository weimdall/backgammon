#--/!\
-- use the command "php bin/console doctrine:schema:update --force" to import schema into database
#--/!\



#------------------------------------------------------------
#        Script MySQL.
#------------------------------------------------------------

DROP DATABASE IF EXISTS backgammon;
CREATE SCHEMA `backgammon` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
GRANT ALL PRIVILEGES ON backgammon.* TO 'symfony'@'localhost';
USE backgammon;

#------------------------------------------------------------
# Table: Mouvement
#------------------------------------------------------------

CREATE TABLE Mouvement(
        ID      int (11) Auto_increment  NOT NULL ,
        De      Int NOT NULL ,
        Depart  Int NOT NULL ,
        ID_Tour Int NOT NULL ,
        PRIMARY KEY (ID )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Joueur
#------------------------------------------------------------

CREATE TABLE Joueur(
        ID        int (11) Auto_increment  NOT NULL ,
        email     Varchar (50) NOT NULL ,
        username  Varchar (25) NOT NULL ,
        password  Varchar (512) NOT NULL ,
        enabled   Bool NOT NULL ,
        locked    Bool NOT NULL ,
        lastLogin TimeStamp NOT NULL ,
        PRIMARY KEY (ID ) ,
        UNIQUE (email ,username )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Partie
#------------------------------------------------------------

CREATE TABLE Partie(
        ID          int (11) Auto_increment  NOT NULL ,
        heure       TimeStamp NOT NULL ,
        ID_Joueur   Int NOT NULL ,
        ID_Joueur_1 Int NOT NULL ,
        PRIMARY KEY (ID )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Tour
#------------------------------------------------------------

CREATE TABLE Tour(
        ID        int (11) Auto_increment  NOT NULL ,
        De1       Int NOT NULL ,
        De2       Int NOT NULL ,
        ID_Partie Int NOT NULL ,
        ID_Joueur Int NOT NULL ,
        PRIMARY KEY (ID )
)ENGINE=InnoDB;


#------------------------------------------------------------
# Table: Amis
#------------------------------------------------------------

CREATE TABLE Amis(
        ID_Joueur   Int NOT NULL ,
        ID_Joueur_1 Int NOT NULL ,
        PRIMARY KEY (ID_Joueur ,ID_Joueur_1 )
)ENGINE=InnoDB;

ALTER TABLE Mouvement ADD CONSTRAINT FK_Mouvement_ID_Tour FOREIGN KEY (ID_Tour) REFERENCES Tour(ID);
ALTER TABLE Partie ADD CONSTRAINT FK_Partie_ID_Joueur FOREIGN KEY (ID_Joueur) REFERENCES Joueur(ID);
ALTER TABLE Partie ADD CONSTRAINT FK_Partie_ID_Joueur_1 FOREIGN KEY (ID_Joueur_1) REFERENCES Joueur(ID);
ALTER TABLE Tour ADD CONSTRAINT FK_Tour_ID_Partie FOREIGN KEY (ID_Partie) REFERENCES Partie(ID);
ALTER TABLE Tour ADD CONSTRAINT FK_Tour_ID_Joueur FOREIGN KEY (ID_Joueur) REFERENCES Joueur(ID);
ALTER TABLE Amis ADD CONSTRAINT FK_Amis_ID_Joueur FOREIGN KEY (ID_Joueur) REFERENCES Joueur(ID);
ALTER TABLE Amis ADD CONSTRAINT FK_Amis_ID_Joueur_1 FOREIGN KEY (ID_Joueur_1) REFERENCES Joueur(ID);
